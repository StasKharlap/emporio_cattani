<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Model\Slider\Grid;

class Type implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * To option slider types array
     * @return array
     */
    public function toOptionArray()
    {
        return \Ulmod\Productslider\Model\ProductSlider::getSliderTypeArray();
    }
}

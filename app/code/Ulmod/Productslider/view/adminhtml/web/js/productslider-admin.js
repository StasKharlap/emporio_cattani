require(['jquery', 'domReady!', 'ulmod/spectrum'], function ($) {
    'use strict';

    /* initial show/hide elements */
    var productsliderEnabled = $('#productslider_general_enabled').val();
    toggleFieldsets(productsliderEnabled);

    /* hide or show fieldsets based on changing extension status */
    /* hide or show elements based on changing zoomtype */
    $('#productslider_general_enabled').change(function () {
        productsliderEnabled = $(this).val();
        toggleFieldsets(productsliderEnabled);
    });

    function toggleFieldsets(status)
    {
        switch (status) {
            case '0':
                $('#productslider_advanced_settings-link').closest('.section-config').hide();
                break;
            case '1':
                $('#productslider_advanced_settings-link').closest('.section-config').show();
                break;
        }
    }

    $('.colorpicker').spectrum({
        showInput: true,
        showAlpha: true,
        showInitial: true,
        showInput: true,
        showButtons: false,
        clickoutFiresChange: true,
        preferredFormat: "rgb",
    });

});

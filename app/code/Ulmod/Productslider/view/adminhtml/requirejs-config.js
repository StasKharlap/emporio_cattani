/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */
var config = {
    paths: {
        'ulmod/spectrum': 'Ulmod_Productslider/js/spectrum'
    },
    shim: {
        'ulmod/spectrum': {
            deps: ['jquery']
        }
    }
};

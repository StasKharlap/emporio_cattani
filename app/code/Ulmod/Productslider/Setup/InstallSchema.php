<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */
 
namespace Ulmod\Productslider\Setup;

use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{
    
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Drop tables if exists
         */
        $installer->getConnection()->dropTable(
            $installer->getTable('ulmod_productslider')
        );
        $installer->getConnection()->dropTable(
            $installer->getTable('ulmod_productslider_products')
        );
        $installer->getConnection()->dropTable(
            $installer->getTable('ulmod_productslider_stores')
        );

        /**
         * Create table 'ulmod_productslider'
         */
          $table = $installer->getConnection()
             ->newTable(
                 $installer->getTable('ulmod_productslider')
             )->addColumn(
                 'slider_id',
                 \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                 null,
                 ['nullable' => false, 'unsigned' => true, 'identity' => true, 'primary' => true],
                 'Slider ID'
             )->addColumn(
                 'title',
                 \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                 256,
                 ['nullable' => false, 'default' => ''],
                 'Slider title'
             )->addColumn(
                 'display_title',
                 \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                 null,
                 ['nullable' => false, 'default' => '1'],
                 'Display title'
             )
                ->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '1'],
                    'Slider status'
                )
                ->addColumn(
                    'description',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    [],
                    'Description'
                )
                ->addColumn(
                    'type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    256,
                    ['nullable' => false, 'default' => ''],
                    'Slider type'
                )
                ->addColumn(
                    'template_type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    256,
                    ['nullable' => false, 'default' => ''],
                    'Template type'
                )
                ->addColumn(
                    'exclude_from_cart',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '0'],
                    'Don\'t display slider on cart page '
                )
                ->addColumn(
                    'exclude_from_checkout',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '0'],
                    'Don\'t display slider on checkout '
                )
                ->addColumn(
                    'location',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    256,
                    ['nullable' => false, 'default' => ''],
                    'Slider location or position'
                )
                ->addColumn(
                    'start_time',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    null,
                    ['nullable' => true],
                    'Slider start time'
                )
                   ->addColumn(
                       'end_time',
                       \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                       null,
                       ['nullable' => true],
                       'Slider end time'
                   )
                    ->addColumn(
                        'navigation_enable',
                        \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        null,
                        ['nullable' => false, 'default' => '1'],
                        'Slider Enable Navigation'
                    )
                    ->addColumn(
                        'navigation_show',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        100,
                        ['nullable' => false, 'default' => 'hover'],
                        'Slider Show Navigation'
                    )
                    ->addColumn(
                        'navigation_position',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        100,
                        ['nullable' => false, 'default' => 'bothsides'],
                        'Slider Navigation Position'
                    )
                   ->addColumn(
                       'navigation_color',
                       \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                       32,
                       ['nullable' => true, 'default' => '#666666'],
                       'Slider Navigation Color'
                   )
                   ->addColumn(
                       'navigation_hover',
                       \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                       32,
                       ['nullable' => true, 'default' => '#666666'],
                       'Slider Navigation Hover Color'
                   )
                   ->addColumn(
                       'pagination_enable',
                       \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                       null,
                       ['nullable' => false, 'default' => '1'],
                       'Slider Enable Pagination'
                   )
                    ->addColumn(
                        'pagination_show',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        100,
                        ['nullable' => false, 'default' => 'always'],
                        'Slider Show Pagination'
                    )
                   ->addColumn(
                       'pagination_color',
                       \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                       32,
                       ['nullable' => true, 'default' => '#293f67'],
                       'Slider Pagination Color'
                   )
                   ->addColumn(
                       'pagination_hover',
                       \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                       32,
                       ['nullable' => true, 'default' => '#cccccc'],
                       'Slider Pagination Hover Color'
                   )
                ->addColumn(
                    'infinite',
                    \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false, 'default' => '1'],
                    'Infinite loop'
                )
                ->addColumn(
                    'slides_to_show',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '5'],
                    'Slides to show'
                )
                ->addColumn(
                    'slides_to_scroll',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '2'],
                    'Slides to scroll'
                )
                ->addColumn(
                    'speed',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '500'],
                    'Speed in ms'
                )
                ->addColumn(
                    'autoplay',
                    \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false, 'default' => '1'],
                    'Autoplay'
                )
                ->addColumn(
                    'autoplay_speed',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '1000'],
                    'Autoplay speed'
                )
                ->addColumn(
                    'rtl',
                    \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false, 'default' => '0'],
                    'Right to left'
                )
                ->addColumn(
                    'breakpoint_large',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '1024'],
                    'Large breakpoint'
                )
                ->addColumn(
                    'large_slides_to_show',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '5'],
                    'Slides to show for large'
                )
                ->addColumn(
                    'large_slides_to_scroll',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '2'],
                    'Slides to scroll for large'
                )
                ->addColumn(
                    'breakpoint_medium',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '768'],
                    'Medium breakpoint'
                )
                ->addColumn(
                    'medium_slides_to_show',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '2'],
                    'Slides to show for medium'
                )
                ->addColumn(
                    'medium_slides_to_scroll',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '1'],
                    'Slides to scroll for Medium'
                )
                ->addColumn(
                    'breakpoint_small',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '480'],
                    'Small breakpoint'
                )
                ->addColumn(
                    'small_slides_to_show',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '2'],
                    'Slides to show for small'
                )
                ->addColumn(
                    'small_slides_to_scroll',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '1'],
                    'Slides to scroll for small'
                )
                ->addColumn(
                    'display_price',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '1'],
                    'Display product price'
                )
                ->addColumn(
                    'display_cart',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '1'],
                    'Display add to cart button'
                )
                ->addColumn(
                    'display_wishlist',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '1'],
                    'Display add to wish list'
                )
                ->addColumn(
                    'display_compare',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '1'],
                    'Display add to compare'
                )
                ->addColumn(
                    'products_number',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Number of products in slider'
                )
                ->addColumn(
                    'enable_swatches',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Enable color swatches'
                )
                ->addColumn(
                    'enable_ajaxcart',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Enable ajax add to cart'
                )
                   ->addIndex(
                       $installer->getIdxName('ulmod_productslider', ['slider_id']),
                       ['slider_id']
                   )
                ->setComment('Ulmod Main Product Slider Table');
               $installer->getConnection()->createTable($table);

        /**
         * Create table 'ulmod_productslider_products'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ulmod_productslider_products')
        )     ->addColumn(
            'slider_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'unsigned' => true, 'primary' => true],
            'Slider ID'
        )->addColumn(
            'product_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'unsigned' => true, 'primary' => true],
            'Product ID'
        )->addColumn(
            'position',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'default' => '0'],
            'Position'
        )->addIndex(
            $installer->getIdxName('ulmod_productslider_products', ['product_id']),
            ['product_id']
        )->addForeignKey(
            $installer->getFkName('ulmod_productslider_products', 'slider_id', 'ulmod_productslider', 'slider_id'),
            'slider_id',
            $installer->getTable('ulmod_productslider'),
            'slider_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('ulmod_productslider_products', 'product_id', 'catalog_product_entity', 'entity_id'),
            'product_id',
            $installer->getTable('catalog_product_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
            ->setComment('Catalog Product To Slider Linkage Table');

        $installer->getConnection()->createTable($table);

        /**
         * Create table 'ulmod_productslider_stores'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ulmod_productslider_stores')
        )->addColumn(
            'slider_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'unsigned' => true, 'primary' => true],
            'Slider ID'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store ID'
        )->addIndex(
            $installer->getIdxName('ulmod_productslider_stores', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName('ulmod_productslider_stores', 'slider_id', 'ulmod_productslider', 'slider_id'),
            'slider_id',
            $installer->getTable('ulmod_productslider'),
            'slider_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('ulmod_productslider_stores', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
            ->setComment('Product slider store table');

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}

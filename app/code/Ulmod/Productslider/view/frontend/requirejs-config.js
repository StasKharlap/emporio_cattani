/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

var config = {
    paths: {
        'slick': 'Ulmod_Productslider/js/slick.min',
        'owlcarousel': 'Ulmod_Productslider/js/owl.carousel.min'
    },
    shim: {
        'slick': {
            deps: ['jquery']
        },
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};

<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Block\Adminhtml\System;

use Magento\Framework\Data\Form\Element\AbstractElement;

class Implementation extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $html = '<div class="notices-wrapper">
      <div class="guide-info">
         <h2 style="font-size: 1.7rem;font-weight: 600;">'.__('Code Inserts Guide').'</h2>
         <p></p>
          <p>'.__('For more flexibility on where to place productslider you can use a CMS template tag, 
         template code or Layout XML code to add this productslider to any page or static block.').'</p>
      </div>      
      <div class="message">
          <strong>'.__('Template Code (template file)').'</strong>
          <p></p>
          <p>'.__('Copy code from below to the template file (between php tags) 
              and replace custom_slider_id with proper slider id').'</p>
         <p> echo $this->getLayout()->createBlock("Ulmod\Productslider\Block\Slider\Items")
                                    ->setSliderId("custom_slider_id")->toHtml(); </p>
      </div>
      <div class="message">
          <strong>'.__('CMS Template Tag (content editor)').'</strong>
          <p></p>
          <p>'.__('Copy code from below to the CMS page or block and
              replace custom_slider_id with proper slider id').'</p>
        <p>  {{block class="Ulmod\Productslider\Block\Slider\Items" slider_id="custom_slider_id"}} </p>
      </div>
      <div class="message">
         <strong>'.__('XML Code (layout xml code)').'</strong>
         <p></p>
          <p>'.__('Copy code from below to the layout XML file and replace 
                custom_slider_id with proper slider id').'</p>
         <p>  &lt;block class="Ulmod\Productslider\Block\Slider\Items"&gt;<br/>
             &nbsp;&nbsp;&lt;action method="setSliderId"&gt;<br/>
                 &nbsp;&nbsp;&nbsp;&nbsp;&lt;argument name="sliderId"
                  xsi:type="string"&gt;your_slider_id&lt;/argument&gt;<br/>
             &nbsp;&nbsp;&lt;/action&gt;<br/>
          &lt;/block>
          </p>
      </div>
     </div>';

        return $html;
    }
}

<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Block\Widget;

class Slider extends \Magento\Framework\View\Element\Template
{
    /**
     * Default template to use for slider widget
     */
    const DEFAULT_SLIDER_TEMPLATE = 'widget/slider.phtml';

    /**
     * set slider widget template
     */
    public function _construct()
    {
        if (!$this->hasData('template')) {
            $this->setData('template', self::DEFAULT_SLIDER_TEMPLATE);
        }
        return parent::_construct();
    }
}

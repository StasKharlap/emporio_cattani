<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Block\Adminhtml\Slider\Edit\Tab;

use Magento\Store\Model\ScopeInterface;
use Magento\Directory\Model\Currency as ModelCurrency;

class Products extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $catalogProductVisibility;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Store\Model\WebsiteFactory
     */
    protected $websiteFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Backend\Helper\Data $helper
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Magento\Backend\Helper\Data $helper,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        array $data = []
    ) {
        $this->productFactory = $productFactory;
        $this->websiteFactory = $websiteFactory;
        $this->coreRegistry = $coreRegistry;
        $this->resource = $resource;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->storeManager = $context->getStoreManager();
        parent::__construct($context, $helper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('products_grid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in slider flag
        $columnId = $column->getId();
        if ($columnId == 'in_slider') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            
            $columnValue = $column->getFilter()->getValue();
            if ($columnValue) {
                $this->getCollection()
                    ->addFieldToFilter(
                        'entity_id',
                        ['in' => $productIds]
                    );
            } elseif (!empty($productIds)) {
                $this->getCollection()
                    ->addFieldToFilter(
                        'entity_id',
                        ['nin' => $productIds]
                    );
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        
        return $this;
    }

    /**
     * Retrieve product slider object
     *
     * @return \Ulmod\Productslider\Model\ProductSlider
     */
    public function getSlider()
    {
        return $this->coreRegistry
            ->registry('product_slider');
    }
    
    /**
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $sliderId = $this->getSlider()->getSliderId();
        if ($sliderId) {
            $this->setDefaultFilter(['in_slider' => 1]);
        }

        $inCatalogIds = $this->catalogProductVisibility
            ->getVisibleInCatalogIds();
            
        $collection = $this->productFactory->create()
            ->getCollection();
        $collection->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('price')
            ->addAttributeToFilter(
                'visibility',
                ['in' => $inCatalogIds]
            )
            ->joinField(
                'position',
                'ulmod_productslider_products',
                'position',
                'product_id=entity_id',
                'slider_id=' . (int)$this->getRequest()
                    ->getParam('id', 0),
                'left'
            );

        $this->setCollection($collection);
        
        $this->getCollection()->addWebsiteNamesToResult();

        return  parent::_prepareCollection();
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_slider',
            [
                'type' => 'checkbox',
                'name' => 'in_slider',
                'index' => 'entity_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction',
                'values' => $this->_getSelectedProducts()
            ]
        );

        $this->addColumn(
            'entity_id',
            [
                 'header' => __('ID'),
                 'index' => 'entity_id',
                 'header_css_class' => 'col-id',
                 'column_css_class' => 'col-id',
                 'sortable' => true
             ]
        );
        
        $this->addColumn(
            'sku',
            [
                 'header' => __('SKU'),
                 'index' => 'sku'
                 ]
        );

         $this->addColumn(
             'name',
             [
                 'header' => __('Name'),
                 'index' => 'name'
             ]
         );
             $this->addColumn(
                 'price',
                 [
                 'header' => __('Price'),
                 'type' => 'currency',
                 'currency_code' => (string)$this->_scopeConfig->getValue(
                     ModelCurrency::XML_PATH_CURRENCY_BASE,
                     ScopeInterface::SCOPE_STORE
                 ),
                 'index' => 'price'
                 ]
             );

        $singleStoreMode = $this->storeManager->isSingleStoreMode();
        if (!$singleStoreMode) {
            $this->addColumn(
                'websites',
                [
                    'header' => __('Websites'),
                    'index' => 'websites',
                    'type' => 'options',
                    'sortable' => false,
                    'options' => $this->websiteFactory->create()
                        ->getCollection()->toOptionHash(),
                    'header_css_class' => 'col-websites',
                    'column_css_class' => 'col-websites'
                ]
            );
        }

        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
                'editable' => true
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * Get selected slider products
     *
     * @return array
     */
    public function getSelectedSliderProducts()
    {
        $slider_id = $this->getRequest()->getParam('id');

        $select = $this->resource->getConnection()->select()
        ->from(
            'ulmod_productslider_products',
            ['product_id', 'position']
        )->where(
            'slider_id = :slider_id'
        );
        $bind = ['slider_id' => (int)$slider_id];

        return $this->resource->getConnection()
            ->fetchPairs($select, $bind);
    }

    /**
     * Get selected products
     *
     * @return array|mixed
     */
    protected function _getSelectedProducts()
    {
        $products = $this->getRequest()
            ->getParam('selected_products');
        if ($products === null) {
            $products = $this->getSlider()
                ->getSelectedSliderProducts();
            return array_keys($products);
        }
        
        return $products;
    }

    /**
     * Retrieve grid reload url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            '*/*/productsgrid',
            ['_current' => true]
        );
    }
}

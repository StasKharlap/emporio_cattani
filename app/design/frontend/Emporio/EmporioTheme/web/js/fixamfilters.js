define([
    'jquery'
], function ($) {
    'use strict';
    return function () {
        $('[data-am-js=shopby-item]').each(function (index, element) {
            var href = $(this).children('a').attr('href');
            $(element).on("click", function () {
                window.location.href = href;
            });
        });
    }
});
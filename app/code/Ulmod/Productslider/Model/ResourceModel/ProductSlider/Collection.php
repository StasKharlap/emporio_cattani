<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Model\ResourceModel\ProductSlider;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Variable.
     *
     * @var string
     */
    protected $_idFieldName = 'slider_id';
    
    /**
     * Initialize resources
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ulmod\Productslider\Model\ProductSlider', 'Ulmod\Productslider\Model\ResourceModel\ProductSlider');
    }

    public function setStoreFilters($storeId)
    {
        $stores = [\Magento\Store\Model\Store::DEFAULT_STORE_ID, $storeId];
        $this->getSelect()
          ->joinLeft(
              ['trs' => $this->getTable(\Ulmod\Productslider\Model\ResourceModel\ProductSlider::SLIDER_STORES_TABLE)],
              'main_table.slider_id = trs.slider_id',
              []
          )
            ->where('trs.store_id IN (?)', $stores);
    }
}

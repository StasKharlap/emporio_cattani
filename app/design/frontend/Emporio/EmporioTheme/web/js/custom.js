
require([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'domReady!'
], function ($, keyboardHandler) {
    'use strict';
    $(document).ready(function(){
        $('.page-header').mage('sticky', {
            container: '#maincontent'
        });

        $('.header-box .header.links').clone().appendTo('#store\\.menu');

    });
    keyboardHandler.apply();
});

/******************** Scroll, Collapse ***********************/
require([
    'jquery'
], function ($) {

    $.fn.extend({
        scrollToMe: function(){
            if($(this).length){
                var top = $(this).offset().top - 100;
                $('html,body').animate({scrollTop: top}, 300);
            }
        },
        scrollToJustMe: function(){
            if($(this).length){
                var top = jQuery(this).offset().top;
                $('html,body').animate({scrollTop: top}, 300);
            }
        }
    });

    $(document).ready(function(){
        var windowScroll_t;
        $(window).scroll(function(){
            clearTimeout(windowScroll_t);
            windowScroll_t = setTimeout(function(){
                if(jQuery(this).scrollTop() > 100){
                    $('#totop').fadeIn();
                }else{
                    $('#totop').fadeOut();
                }
            }, 500);
        });

        $('#totop').off("click").on("click",function(){
            $('html, body').animate({scrollTop: 0}, 600);
        });


        $('.nav-sections').click(function (event){
            var mobli = $('#store\\.menu');
            if (!mobli.is(event.target)
                && mobli.has(event.target).length === 0) {
                event.stopPropagation();
            }
        });

            $('a.level-top').each(function(){
                $(this).click(function() {
                    if($(this).hasClass('ui-state-active')) {
                        $(this).parent().toggleClass('ui-menu-item-open');
                        $(this).closest('.parent.ui-menu-item').find('.level0.submenu').toggleClass('submenu-open');
                        $(this).parent().prevAll().toggleClass('ui-menu-item-hide');
                        $(this).parent().nextAll().toggleClass('ui-menu-item-hide');
                        $(this).closest('.nav-sections-item-content').find('.header.links').toggleClass('no-border-open-submenu');
                    } else {
                        $(this).parent().toggleClass('ui-menu-item-open');
                        $(this).closest('.parent.ui-menu-item').find('.level0.submenu').toggleClass('submenu-open');
                        $(this).parent().prevAll().toggleClass('ui-menu-item-hide');
                        $(this).parent().nextAll().toggleClass('ui-menu-item-hide');
                        $(this).closest('.nav-sections-item-content').find('.header.links').toggleClass('no-border-open-submenu');
                    }

                });
            });

    });

});


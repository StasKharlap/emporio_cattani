define([
    'jquery'
], function ($) {
    'use strict';
    return function () {
        $('.product-item-info').each(function (index, element) {
            var child = $(this).find('.stock.unavailable');
            $(this).prepend(child);
        });
    }
});

<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Block\Adminhtml\Slider\Edit\Tab;

use Ulmod\Productslider\Model\ProductSlider;
use Magento\Store\Model\ScopeInterface as Scope;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Config\Model\Config\Source\Yesno as SourceYesno;
use Magento\Store\Model\System\Store as SystemStore;
        
class General extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Config path to default slider settings
     */
    const XML_PATH_PRODUCT_SLIDER_DEFAULT_VALUES = 'productslider/slider_settings/' ;

    /**
     * @var SourceYesno
     */
    protected $yesNo;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var SystemStore
     */
    protected $systemStore;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param SourceYesno $yesNo
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        SourceYesno $yesNo,
        SystemStore $systemStore,
        array $data = []
    ) {
        $this->yesNo = $yesNo;
        $this->scopeConfig = $context->getScopeConfig();
        $this->systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post'
                ]
            ]
        );

        $productSlider = $this->_coreRegistry
            ->registry('product_slider');
        $yesno = $this->yesNo->toOptionArray();

        $fieldset = $form->addFieldset(
            'slider_fieldset_general',
            ['legend' => __('General')]
        );
        
        $prodSliderId = $productSlider->getId();
        if ($prodSliderId) {
            $fieldset->addField(
                'slider_id',
                'hidden',
                [
                    'name' => 'slider_id'
                ]
            );
        }

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'title' => __('Title'),
                'label' => __('Title'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Slider status'),
                'title' => __('Slider status'),
                'name' => 'status',
                'options' => ProductSlider::getStatusArray(),
                'disabled' => false,
            ]
        );

        /**
         * Check if single store mode
         */
        $singleStore = $this->_storeManager->isSingleStoreMode();
        if (!$singleStore) {
            $field = $fieldset->addField(
                'store_id',
                'multiselect',
                [
                    'name' => 'stores[]',
                    'label' => __('Store view'),
                    'title' => __('Store view'),
                    'values' => $this->systemStore
                        ->getStoreValuesForForm(false, true),
                    'required' => true,
                ]
            );

            /** @var \Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element $renderer */
            $renderer = $this->getLayout()->createBlock(
                'Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element'
            );
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField(
                'store_id',
                'hidden',
                [
                    'name' => 'stores[]',
                    'value' => $this->_storeManager->getStore(true)
                        ->getId()
                ]
            );
        }
        
        $dateFormat = $this->_localeDate->getDateFormat(
            \IntlDateFormatter::SHORT
        );
        $timeFormat = $this->_localeDate->getTimeFormat(
            \IntlDateFormatter::SHORT
        );
        $fieldset->addField(
            'start_time',
            'date',
            [
                'name' => 'start_time',
                'label' => __('Start time'),
                'title' => __('Start time'),
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'note' => $this->_localeDate->getDateTimeFormat(\IntlDateFormatter::SHORT),
            ]
        );
        $fieldset->addField(
            'end_time',
            'date',
            [
                'name' => 'end_time',
                'label' => __('End time'),
                'title' => __('Start time'),
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'note' => $this->_localeDate
                    ->getDateTimeFormat(\IntlDateFormatter::SHORT),
            ]
        );
        
        $fieldset = $form->addFieldset(
            'slider_fieldset_appearance',
            ['legend' => __('Appearance')]
        );
        
        $fieldset->addField(
            'type',
            'select',
            [
                'label' => __('Slider type'),
                'title' => __('Slider type'),
                'name' => 'type',
                'required' => true,
                'values' => ProductSlider::getSliderTypeArray(),
                'note' => __('Auto related products available only on product page location.'),
            ]
        );
        
        $fieldset->addField(
            'template_type',
            'select',
            [
                'label' => __('Template Type'),
                'title' => __('Template Type'),
                'name' => 'template_type',
                'required' => true,
                'values' => ProductSlider::getTemplateTypeArray(),
                'note' => __('Select template types : slick carousel slider, owl carousel slider or grid'),
            ]
        );
        
        $fieldset->addField(
            'location',
            'select',
            [
                'label' => __('Slider position'),
                'title' => __('Slider position'),
                'name' => 'location',
                'required' => false,
                'values' => ProductSlider::getSliderLocations()
            ]
        );
        $fieldset->addField(
            'products_number',
            'text',
            [
                'name' => 'products_number',
                'label' => __('Max Products number'),
                'title' => __('Max Products number'),
                'note' => __('Max Number of products displayed in slider. Default is 30 products.'),
            ]
        );
        $fieldset->addField(
            'display_title',
            'select',
            [
                'label' => __('Display slider title'),
                'title' => __('Display slider title'),
                'name' => 'display_title',
                'values' => $yesno
            ]
        );
        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => __('Slider Description'),
                'title' => __('Slider Description'),
                'note' => __('This description will shown under title.'),
            ]
        );
        $fieldset->addField(
            'exclude_from_cart',
            'select',
            [
                'label' => __('Exclude from cart'),
                'title' => __('Exclude from cart'),
                'note'  => __('Don\'t display sliders on cart page'),
                'name' => 'exclude_from_cart',
                'values' => $yesno
            ]
        );

        $fieldset->addField(
            'exclude_from_checkout',
            'select',
            [
                'label' => __('Exclude from checkout'),
                'title' => __('Exclude from cart'),
                'note'  => __('Don\'t display sliders on checkout'),
                'name' => 'exclude_from_checkout',
                'values' => $yesno
            ]
        );
        
        $fieldset = $form->addFieldset(
            'slider_fieldset_products',
            ['legend' => __('Products')]
        );
        $fieldset->addField(
            'display_price',
            'select',
            [
                'label' => __('Display price'),
                'title' => __('Display price'),
                'name' => 'display_price',
                'values' => $yesno
            ]
        );

        $fieldset->addField(
            'display_cart',
            'select',
            [
                'label' => __('Display cart'),
                'title' => __('Display add to cart button'),
                'name' => 'display_cart',
                'values' => $yesno
            ]
        );

        $fieldset->addField(
            'display_wishlist',
            'select',
            [
                'label' => __('Display wishlist'),
                'title' => __('Display add to wish list'),
                'name' => 'display_wishlist',
                'values' => $yesno
            ]
        );

        $fieldset->addField(
            'display_compare',
            'select',
            [
                'label' => __('Display compare'),
                'title' => __('Display add to compare'),
                'name' => 'display_compare',
                'values' => $yesno
            ]
        );

        $fieldset->addField(
            'enable_ajaxcart',
            'select',
            [
                'label' => __('Enable ajax add to cart'),
                'title' => __('Enable ajaxcart'),
                'name' => 'enable_ajaxcart',
                'values' => $yesno
            ]
        );
    
        $fieldset->addField(
            'enable_swatches',
            'select',
            [
                'label' => __('Enable swatches'),
                'title' => __('Enable swatches'),
                'name' => 'enable_swatches',
                'values' => $yesno
                ]
        );

       // set default values
        $defaultData = [
            'status' => 1,
            'products_number' => 8,
            'display_price' => 1,
            'display_cart' => 1,
            'display_compare' => 1,
            'display_wishlist' => 1,
            'enable_ajaxcart' => 1,
        ];

        if (!$productSlider->getId()) {
            $productSlider->addData($defaultData);
        }
        if ($productSlider->getData()) {
            $form->setValues($productSlider->getData());
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }
}

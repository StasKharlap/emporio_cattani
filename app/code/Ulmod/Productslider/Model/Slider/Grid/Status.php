<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Model\Slider\Grid;

class Status implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * To option slider statuses array
     * @return array
     */
    public function toOptionArray()
    {
        return \Ulmod\Productslider\Model\ProductSlider::getStatusArray();
    }
}

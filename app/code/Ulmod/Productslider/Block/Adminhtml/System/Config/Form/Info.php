<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Block\Adminhtml\System\Config\Form;

use Magento\Store\Model\ScopeInterface;

/**
 * Admin Productslider configuration information block
 */
class Info extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $moduleList;

    /**
     * @param \Magento\Framework\Module\ModuleListInterface $moduleList
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->moduleList       = $moduleList;
    }

    /**
     * Return info block html
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $m = $this->moduleList->getOne($this->getModuleName());
        $version  = $m['setup_version'];
        $prodsliderimgpath = 'http://www.ulmod.com/pub/media/ulmod_info/um-prodslider.png';
        $html = <<<HTML
        <div style="background:url('$prodsliderimgpath') no-repeat scroll 15px 11px #f8f8f8;
         border:1px solid #ccc; margin:5px 0; padding:15px 15px 15px 130px;background-size: 100px 100px;">
          <p>
       <strong class="um-product-info">
             <span class="um-prod-name">Product Slider</span>
       </strong>
          </p>
          <p>
         Showcase your new(latest), best seller, most viewed, sale, featured or auto related
          products in the most vivid and interactive way and drive more of your visitors’ 
          attention to your best and most profitable products in your store.
          </p>      
          <p>
           If you have any questions, email us at <a href="mailto:support@ulmod.com">support@ulmod.com</a>.
           </p>
        </div>
HTML;
        return $html;
    }
}

<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */

namespace Ulmod\Productslider\Block\Adminhtml\Slider\Edit\Tab;

use Ulmod\Productslider\Model\ProductSlider;
use \Magento\Store\Model\ScopeInterface as Scope;

class Advanced extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Config path for default slider settings
     */
    const XML_PATH_PRODUCT_SLIDER_DEFAULT_VALUES = 'productslider/slider_settings/' ;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $yesNo;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Config\Model\Config\Source\Yesno $yesNo
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Source\Yesno $yesNo,
        array $data = []
    ) {
        $this->yesNo = $yesNo;
        $this->scopeConfig = $context->getScopeConfig();
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create();

        $productSlider = $this->_coreRegistry->registry('product_slider');
        $yesno = $this->yesNo->toOptionArray();
     
        $fieldset = $form->addFieldset(
            'slider_fieldset_navigation',
            ['legend' => __('Slider Navigation')]
        );
        
        $fieldset->addField(
            'navigation_enable',
            'select',
            [
                'name' => 'navigation_enable',
                'label' => __('Enable Navigation'),
                'title' => __('Enable Navigation'),
                'note' => __('If Yes, Navigation Arrows will show'),
                'values' => $yesno
            ]
        );
        $fieldset->addField(
            'navigation_show',
            'select',
            [
                'name' => 'navigation_show',
                'label' => __('Show Navigation Arrows'),
                'title' => __('Show Navigation Arrows'),
                'values' => ProductSlider::getNavigationShowArray()
            ]
        );
        
        $fieldset->addField(
            'navigation_position',
            'select',
            [
                'name' => 'navigation_position',
                'label' => __('Navigation Arrows Position'),
                'title' => __('Navigation Arrows Position'),
                'values' => ProductSlider::getNavigationPositionArray()
            ]
        );
        $fieldset->addField(
            'navigation_color',
            'text',
            [
                'name' => 'navigation_color',
                'label' => __('Navigation Arrows Color'),
                'title' => __('Navigation Arrows Color'),
                'class' => 'colorpicker',
                'style' => 'width: 196px; height: 30px'
            ]
        );
        $fieldset->addField(
            'navigation_color_hover',
            'text',
            [
                'name' => 'navigation_color_hover',
                'label' => __('Navigation Arrows Hover Color'),
                'title' => __('Navigation Arrows Hover Color'),
                'class' => 'colorpicker',
                'style' => 'width: 196px; height: 30px'
            ]
        );
     
        $fieldset = $form->addFieldset(
            'slider_fieldset_pagination',
            ['legend' => __('Slider Pagination')]
        );
        
        $fieldset->addField(
            'pagination_enable',
            'select',
            [
                'name' => 'pagination_enable',
                'label' => __('Enable Pagination'),
                'title' => __('Enable Pagination'),
                'note' => __('If Yes, Pagination Dots will show'),
                'values' => $yesno
            ]
        );
        $fieldset->addField(
            'pagination_show',
            'select',
            [
                'name' => 'pagination_show',
                'label' => __('Show Pagination Dots'),
                'title' => __('Show Pagination Dots'),
                'values' => ProductSlider::getPaginationShowArray()
            ]
        );

        $fieldset->addField(
            'pagination_color',
            'text',
            [
                'name' => 'pagination_color',
                'label' => __('Pagination Dots Color '),
                'title' => __('Pagination Dots Color'),
                'class' => 'colorpicker',
                'style' => 'width: 196px; height: 30px'
            ]
        );

        $fieldset = $form->addFieldset(
            'slider_fieldset_effects',
            ['legend' => __('Slider Effects')]
        );
        
        $fieldset->addField(
            'infinite',
            'select',
            [
                'name' => 'infinite',
                'label' => __('Infinite loop sliding'),
                'title' => __('Infinite loop sliding'),
                'note' => __('Inifnity loop'),
                'values' => $yesno
            ]
        );

        $fieldset->addField(
            'slides_to_show',
            'text',
            [
                'name' => 'slides_to_show',
                'label' => __('Slides to show'),
                'title' => __('Number of slides to show')
            ]
        );

        $fieldset->addField(
            'slides_to_scroll',
            'text',
            [
                'name' => 'slides_to_scroll',
                'label' => __('Slides to scroll'),
                'title' => __('Number of slides to scroll')
            ]
        );

        $fieldset->addField(
            'speed',
            'text',
            [
                'name' => 'speed',
                'label' => __('Speed'),
                'title' => __('Speed'),
                'note' => __('Slide animation speed')
            ]
        );

        $fieldset->addField(
            'autoplay',
            'select',
            [
                'name' => 'autoplay',
                'label' => __('Autoplay'),
                'title' => __('Autoplay'),
                'values' => $yesno
            ]
        );

        $fieldset->addField(
            'autoplay_speed',
            'text',
            [
                'name' => 'autoplay_speed',
                'label' => __('Autoplay speed'),
                'title' => __('Autoplay speed')
            ]
        );

        $fieldset->addField(
            'rtl',
            'select',
            [
                'name' => 'rtl',
                'label' => __('Right to left'),
                'title' => __('Right to left'),
                'note' => __('Change the slider direction to become right-to-left'),
                'values' => $yesno
            ]
        );

        $fieldset = $form->addFieldset(
            'slider_fieldset_large',
            ['legend' => __('Large display settings (For Responsive web design)')]
        );

        $fieldset->addField(
            'breakpoint_large',
            'text',
            [
                'name' => 'breakpoint_large',
                'label' => __('Breakpoint large width)'),
                'title' => __('Breakpoint large width')
            ]
        );

        $fieldset->addField(
            'large_slides_to_show',
            'text',
            [
                'name' => 'large_slides_to_show',
                'label' => __('Slides to show on large breakpoint'),
                'title' => __('Slides to show on large breakpoint')
            ]
        );

        $fieldset->addField(
            'large_slides_to_scroll',
            'text',
            [
                'name' => 'large_slides_to_scroll',
                'label' => __('Slides to scroll on large breakpoint'),
                'title' => __('Slides to scroll on large breakpoint')
            ]
        );

        $fieldset = $form->addFieldset(
            'slider_fieldset_medium',
            ['legend' => __('Medium display settings (For Responsive web design)')]
        );

        $fieldset->addField(
            'breakpoint_medium',
            'text',
            [
                'name' => 'breakpoint_medium',
                'label' => __('Breakpoint medium width'),
                'title' => __('Breakpoint medium width')
            ]
        );

        $fieldset->addField(
            'medium_slides_to_show',
            'text',
            [
                'name' => 'medium_slides_to_show',
                'label' => __('Slides to show on medium breakpoint'),
                'title' => __('Slides to show on medium breakpoint')
            ]
        );

        $fieldset->addField(
            'medium_slides_to_scroll',
            'text',
            [
                'name' => 'medium_slides_to_scroll',
                'label' => __('Slides to scroll on medium breakpoint'),
                'title' => __('Slides to scroll on medium breakpoint')
            ]
        );

        $fieldset = $form->addFieldset(
            'slider_fieldset_small',
            ['legend' => __('Small display settings (For Responsive web design)')]
        );

        $fieldset->addField(
            'breakpoint_small',
            'text',
            [
                'name' => 'breakpoint_small',
                'label' => __('Breakpoint small width'),
                'title' => __('Breakpoint small width')
            ]
        );

        $fieldset->addField(
            'small_slides_to_show',
            'text',
            [
                'name' => 'small_slides_to_show',
                'label' => __('Slides to show on small breakpoint'),
                'title' => __('Slides to show on small breakpoint')
            ]
        );

        $fieldset->addField(
            'small_slides_to_scroll',
            'text',
            [
                'name' => 'small_slides_to_scroll',
                'label' => __('Slides to scroll on small breakpoint'),
                'title' => __('Slides to scroll on small breakpoint')
            ]
        );
        
    // set default values
        $defaultData = [
            'navigation_enable' => 1,
            'navigation_color' => '#666666',
            'navigation_hover' => '#666666',
            'pagination_enable' => 1,
            'pagination_show' => 'always',
            'pagination_color' => '#293f67',
            'pagination_color' => '#cccccc',
            'infinite' => 0,
            'slides_to_show' => 5,
            'slides_to_scroll' => 2,
            'pagination_enable' => 1,
            'speed' => 500,
            'autoplay' => 1,
            'autoplay_speed' => 1000,
            'breakpoint_large' => 1024,
            'large_slides_to_show' => 5,
            'large_slides_to_scroll' => 2,
            'breakpoint_medium' => 768,
            'medium_slides_to_show' => 2,
            'medium_slides_to_scroll' => 1,
            'breakpoint_small' => 480,
            'small_slides_to_show' => 2,
            'small_slides_to_scroll' => 1,
        ];

        if (!$productSlider->getId()) {
            $productSlider->addData($defaultData);
        }
    
        if ($productSlider->getData()) {
            $form->setValues($productSlider->getData());
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }
}

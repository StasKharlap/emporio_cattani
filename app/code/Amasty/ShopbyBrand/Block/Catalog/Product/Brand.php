<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_ShopbyBrand
 */


namespace Amasty\ShopbyBrand\Block\Catalog\Product;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Brand extends \Magento\Catalog\Block\Product\AbstractProduct
{
    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var \Amasty\ShopbyBrand\Helper\Data
     */
    private $helper;

    /**
     * @var \Amasty\ShopbyBase\Model\OptionSettingFactory
     */
    private $modelOptionSettingFactory;

    /**
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    private $stockHelper;

    public function __construct(
        Context $context,
        CollectionFactory $productCollectionFactory,
        \Amasty\ShopbyBrand\Helper\Data $helper,
        \Amasty\ShopbyBase\Model\OptionSettingFactory $modelOptionSettingFactory,
        \Magento\CatalogInventory\Helper\Stock $stockHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->productCollectionFactory = $productCollectionFactory;
        $this->helper = $helper;
        $this->modelOptionSettingFactory = $modelOptionSettingFactory;
        $this->stockHelper = $stockHelper;
    }

    /**
     * @return \Amasty\ShopbyBase\Model\OptionSetting[]
     */
    public function getBrands()
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->_coreRegistry->registry('product');
        $attributeCode = $this->helper->getBrandAttributeCode();
        $attributeValue = $product->getData($attributeCode);

        $attributeOptionText = '';
        $attribute = $product->getResource()->getAttribute($attributeCode);
        if ($attribute && $attribute->usesSource()) {
            $attributeOptionText = $attribute->getSource()->getOptionText($attributeValue);
        }

        if (!$attributeValue) {
            return $this;
        }
        $attributeValue = explode(',', $attributeValue);

        $brands = [];
        foreach ($attributeValue as $value) {
            $optionSetting = $this->modelOptionSettingFactory->create()->load($value, 'value');
            if (!$optionSetting->getTitle()) {
                $optionSetting->setTitle($attributeOptionText);
            }

            $brands[] = $optionSetting;
        }

        return $brands;
    }

    /**
     * @return string
     */
    public function _toHtml()
    {
        return parent::_toHtml();
    }
}

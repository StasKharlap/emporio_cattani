<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */
 
namespace Ulmod\Productslider\Model\Config\Source;

class Slider implements \Magento\Framework\Option\ArrayInterface
{
   /**
    * @var \Ulmod\Productslider\Model\ProductSliderFactory
    */
    protected $productsliderFactory;
    
    /**
     * @param \Ulmod\Productslider\Model\ProductSliderFactory $productsliderFactory
     */
    public function __construct(
        \Ulmod\Productslider\Model\ProductSliderFactory $productsliderFactory
    ) {
        $this->productsliderFactory = $productsliderFactory;
    }
    
    /**
     * Get sliders
     * @return void
     */
    public function getSliders()
    {
        $sliderModel = $this->productsliderFactory->create();
        return $sliderModel->getCollection()->getData();
    }
    
    /**
     * To option array
     * @return array
     */
    public function toOptionArray()
    {
        $sliders = [];
        foreach ($this->getSliders() as $slider) {
            array_push($sliders, [
                'value' => $slider['slider_id'],
                'label' => $slider['title']
            ]);
        }
        return $sliders;
    }
}

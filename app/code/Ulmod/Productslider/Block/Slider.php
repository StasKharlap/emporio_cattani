<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */
 
namespace Ulmod\Productslider\Block;

use Ulmod\Productslider\Model\ProductSlider;

class Slider extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    /**
     * Config path to enable extension
     */
    const XML_PATH_PRODUCT_SLIDER_STATUS = "um_productslider/general/enable_productslider";
    
    protected $swatchesVld = false;
    protected $ajaxcartVld = false;
    
    /**
     * Main template container
     */
    protected $_template = 'Ulmod_Productslider::slider.phtml';
    
    /**
     * Product slider collection factory
     *
     * @var \Ulmod\Productslider\Model\ResourceModel\ProductSlider\CollectionFactory
     */
    protected $sliderCollectionFactory;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $layoutConfig;
    
    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Ulmod\Productslider\Model\ResourceModel\ProductSlider\CollectionFactory $sliderCollectionFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Ulmod\Productslider\Model\ResourceModel\ProductSlider\CollectionFactory $sliderCollectionFactory,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->sliderCollectionFactory = $sliderCollectionFactory;
        $this->scopeConfig = $context->getScopeConfig();
        $this->layoutConfig = $context->getLayout();
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    
    /**
     * Render block HTML
     * if extension is enabled then render HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->scopeConfig->getValue(
            self::XML_PATH_PRODUCT_SLIDER_STATUS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES
        )) {
            return parent::_toHtml();
        }
        return false;
    }
    
    /**
     *  Set slider location
     *
     * @return void
     */
    public function setSliderLocation($location, $hide = false)
    {
        $todayDateTime = $this->_localeDate->date()->format('Y-m-d H:i:s');
        $cartHandles = ['0'=>'checkout_cart_index'];
        $checkoutHandles = ['0'=>'checkout_index_index','1'=>'checkout_onepage_failure',
                             "2"=>'checkout_onepage_success'];
        $currentHandles = $this->layoutConfig->getUpdate()->getHandles();
        
        // Get data without start/end time
        /**
         *  @var \Ulmod\Productslider\Model\ResourceModel\ProductSlider\Collection $sliderCollection
         */
        $sliderCollection = $this->sliderCollectionFactory->create();
        $sliderCollection->setStoreFilters($this->_storeManager->getStore()->getId());
        $sliderCollection->addFieldToFilter('status', ProductSlider::STATUS_ENABLED)
            ->addFieldToFilter('start_time', ['null' => true])
            ->addFieldToFilter('end_time', ['null' => true]);
            
        // Check to exclude from cart page
        if (array_intersect($cartHandles, $currentHandles)) {
            $sliderCollection->addFieldToFilter('exclude_from_cart', 0);
        }
        // Check to exclude from checkout
        if (array_intersect($checkoutHandles, $currentHandles)) {
            $sliderCollection->addFieldToFilter('exclude_from_checkout', 0);
        }
        $sliderCollection->addFieldToFilter('location', $location);
 
        /** Get data with start/end time
         *
         *  @var \Ulmod\Productslider\Model\ResourceModel\ProductSlider\Collection $sliderCollectionTimer
         */
        $sliderCollectionTimer = $this->sliderCollectionFactory->create();
        $sliderCollectionTimer->setStoreFilters($this->_storeManager->getStore()->getId());
        $sliderCollectionTimer->addFieldToFilter('status', ProductSlider::STATUS_ENABLED)
            ->addFieldToFilter('start_time', ['lteq' => $todayDateTime ])
            ->addFieldToFilter(
                'end_time',
                [
                                    'or' => [
                                        0 => ['date' => true, 'from' => $todayDateTime],
                                        1 => ['is' => new \Zend_Db_Expr('null')],
                                    ]
                ]
            );
        // Check to exclude from cart page
        if (array_intersect($cartHandles, $currentHandles)) {
            $sliderCollectionTimer->addFieldToFilter('exclude_from_cart', 0);
        }
        // Check to exclude from checkout
        if (array_intersect($checkoutHandles, $currentHandles)) {
            $sliderCollectionTimer->addFieldToFilter('exclude_from_checkout', 0);
        }
         $sliderCollectionTimer->addFieldToFilter('location', $location);
  
        $this->setSlider($sliderCollection);
        $this->setSlider($sliderCollectionTimer);
    }
    
    /**
     *  Add child sliders block
     *
     * @param \Ulmod\Productslider\Model\ResourceModel\ProductSlider\Collection $sliderCollection
     *
     * @return $this
     */
    public function setSlider($sliderCollection)
    {
        foreach ($sliderCollection as $slider) :
            $this->coreRegistry->unregister('slider_id');
            $this->coreRegistry->register('slider_id', $slider->getId());
            $this->coreRegistry->unregister('enable_swatches');
            if ($slider->getEnableSwatches()) {
                $this->coreRegistry->register('enable_swatches', 1);
                $this->swatchesVld = true;
            }
            if ($slider->getEnableAjaxcart()) {
                $this->ajaxcartVld = true;
            }
            $this->append($this->getLayout()
                                ->createBlock('\Ulmod\Productslider\Block\Slider\Items')
                                ->setSlider($slider));
        endforeach;
        $this->addSwatchesCss();
        $this->addAjaxCartJs();
        return $this;
    }
    
    /**
     *  Add swatches css
     *
     * @return $this
     */
    public function addSwatchesCss()
    {
        if (!$this->swatchesVld) {
            return false;
        }
        $swatchesHandles = ['0'=>'catalog_category_view',
                            '1'=>'catalogsearch_advanced_result',
                            "2"=>'catalogsearch_result_index'
                            ];
        $currentHandles = $this->layoutConfig->getUpdate()->getHandles();
        if (!$this->getLayout()->getBlock("swatches-css")) {
            if (!array_intersect($swatchesHandles, $currentHandles)) {
                $block = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template', 'swatches-css')
                        ->setTemplate('Ulmod_Productslider::swatches/css.phtml');
                $this->getLayout()->setChild('head.additional', $block->getNameInlayout(), 'swatches-css');
            }
        }
    }
    
    /**
     *  Add ajax cart js
     *
     * @return $this
     */
    public function addAjaxCartJs()
    {
        if (!$this->ajaxcartVld) {
            return false;
        }
        $swatchesHandles = ['0'=>'catalog_category_view',
                            '1'=>'catalogsearch_advanced_result',
                            "2"=>'catalogsearch_result_index'
                            ];
        $currentHandles = $this->layoutConfig->getUpdate()->getHandles();
        if (!$this->getLayout()->getBlock("ajaxcart-js")) {
            if (!array_intersect($swatchesHandles, $currentHandles)) {
                $block = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template', 'ajaxcart-js')
                        ->setTemplate('Ulmod_Productslider::ajax/js.phtml');
                $this->getLayout()->setChild('content', $block->getNameInlayout(), 'ajaxcart-js');
            }
        }
    }
}

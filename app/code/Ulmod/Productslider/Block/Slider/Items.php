<?php
/**
 * Productslider
 *
 * @package     Ulmod_Productslider
 * @author      Ulmod <support@ulmod.com>
 * @copyright   Copyright (c) 2016 Ulmod (http://www.ulmod.com/)
 * @license     http://www.ulmod.com/license-agreement.html
 */
 
namespace Ulmod\Productslider\Block\Slider;

class Items extends \Magento\Catalog\Block\Product\AbstractProduct
{
    /**
     * Max number of products in slider
     */
    const MAX_PRODUCTS_COUNT = 30;
    
    /**
     * Product slider template
     */
    protected $_template = 'Ulmod_Productslider::slider/items.phtml';
    
    protected $renderer;
    protected $productsNumber;
    
    /**
     * @var \Ulmod\Productslider\Model\ProductSliderFactory
     */
    protected $sliderFactory;
    
    /**
     * @var int
     */
    protected $_sliderId;
    
    /**
     * @var \Ulmod\Productslider\Model\ProductSlider
     */
    protected $slider;
    
    /**
     * Events type factory
     *
     * @var \Magento\Reports\Model\Event\TypeFactory
     */
    protected $eventTypeFactory;
    
    /**
     * @var \Magento\Reports\Model\Event\TypeFactory
     */
    protected $catalogProductVisibility;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productsCollectionFactory;
    
    /**
     * Product reports collection factory
     *
     * @var \Magento\Reports\Model\ResourceModel\Product\CollectionFactory
     */
    protected $reportsCollectionFactory;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;
    
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $urlHelper;
    
    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productsCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $reportsCollectionFactory
     * @param \Ulmod\Productslider\Model\ProductsliderFactory $productsliderFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Reports\Model\Event\TypeFactory $eventTypeFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productsCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $reportsCollectionFactory,
        \Ulmod\Productslider\Model\ProductSliderFactory $productSliderFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Reports\Model\Event\TypeFactory $eventTypeFactory,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        array $data = []
    ) {
        $this->productCollectionFactory = $productsCollectionFactory;
        $this->reportsCollectionFactory = $reportsCollectionFactory;
        $this->catalogProductVisibility = $catalogProductVisibility;
        $this->sliderFactory = $productSliderFactory;
        $this->dateTime = $dateTime;
        $this->eventTypeFactory = $eventTypeFactory;
        $this->storeManager = $context->getStoreManager();
        $this->urlHelper = $urlHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get featured slider products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getSliderFeaturedProducts($collection)
    {
        $collection = $this->_addProductAttributesAndPrices($collection);
        $collection->getSelect()
            ->join(
                ['slider_products' => $collection->getTable('ulmod_productslider_products')],
                'e.entity_id = slider_products.product_id AND slider_products.slider_id = '.$this->getSliderId(),
                ['position']
            )
                  ->order('slider_products.position');
        $collection->addStoreFilter($this->getStoreId())
                    ->setPageSize($this->getProductsCount())
                    ->setCurPage(1);
        $this->productsNumber = $this->getProductsCount() - $collection->count();
        
        return $collection;
    }
    
    /**
     * Get slider products
     *
     * @param $type
     * @return Collection|\Magento\Catalog\Model\ResourceModel\Product\Collection|string
     */
    public function getSliderProducts($type)
    {
        $collection = "";
        switch ($type) {
            case 'new':
                $collection =  $this->_getNewProducts(
                    $this->productCollectionFactory->create()
                );
                break;
            case 'bestsellers':
                $collection = $this->_getBestsellersProducts(
                    $this->productCollectionFactory->create()
                );
                break;
            case 'mostviewed':
                $collection =  $this->_getMostViewedProducts(
                    $this->productCollectionFactory->create()
                );
                break;
            case 'onsale':
                $collection =  $this->_getOnSaleProducts(
                    $this->productCollectionFactory->create()
                );
                break;
            case 'featured':
                $collection =  $this->_getSliderFeaturedProducts(
                    $this->productCollectionFactory->create()
                );
                break;
            case 'autorelated':
                $collection =  $this->_getAutoRelatedProducts(
                    $this->productCollectionFactory->create()
                );
                break;
        }
        return $collection;
    }
    
    /**
     * Get product slider items based on type
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getNewProducts($collection)
    {
        $collection->setVisibility(
            $this->catalogProductVisibility->getVisibleInCatalogIds()
        );
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addAttributeToFilter(
                'news_from_date',
                ['date' => true, 'to' => $this->getEndOfDayDate()],
                'left'
            )
            ->addAttributeToFilter(
                'news_to_date',
                [
                    'or' => [
                        0 => ['date' => true, 'from' => $this->getStartOfDayDate()],
                        1 => ['is' => new \Zend_Db_Expr('null')],
                    ]
                ],
                'left'
            )
            ->addAttributeToSort(
                'news_from_date',
                'desc'
            )
             ->addStoreFilter($this->getStoreId())
             ->setPageSize($this->getProductsCount())
             ->setCurPage(1);
             
        return $collection;
    }

    /**
     * Get best selling products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getBestsellersProducts($collection)
    {
        $collection->setVisibility(
            $this->catalogProductVisibility->getVisibleInCatalogIds()
        );
        $collection = $this->_addProductAttributesAndPrices($collection);
        $collection->getSelect()
            ->join(
                ['bestsellers' => $collection->getTable('sales_bestsellers_aggregated_yearly')],
                'e.entity_id = bestsellers.product_id AND bestsellers.store_id = '.$this->getStoreId(),
                ['qty_ordered','rating_pos']
            )
            ->order('rating_pos');
        $collection->addStoreFilter($this->getStoreId())
                    ->setPageSize($this->getProductsCount())
                    ->setCurPage(1);
                    
        return $collection;
    }
    
    /**
     * Get most viewed products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return Collection
     */
    protected function _getMostViewedProducts($collection)
    {
        $reportCollection = $this->reportsCollectionFactory->create();
        
        // Getting event type id for catalog_product_view event
        $eventTypes = $this->eventTypeFactory->create()
            ->getCollection();
        foreach ($eventTypes as $eventType) {
            if ($eventType->getEventName() == 'catalog_product_view') {
                $productViewEvent = (int)$eventType->getId();
                break;
            }
        }
        
        $inCatalogIds = $this->catalogProductVisibility->getVisibleInCatalogIds();
        $collection->setVisibility($inCatalogIds);
        
        $reportEvent = $reportCollection->getTable('report_event');
        $prodEntTabname = $reportCollection->getProductEntityTableName();
        $prodAttrSetid = $reportCollection->getProductAttributeSetId();
        $collection = $this->_addProductAttributesAndPrices($collection);
        $collection->getSelect()->reset()
            ->from(
                ['report_table_views' => $reportEvent],
                ['views' => 'COUNT(report_table_views.event_id)']
            )->join(
                ['e' => $prodEntTabname],
                $reportCollection->getConnection()->quoteInto(
                    'e.entity_id = report_table_views.object_id',
                    $prodAttrSetid
                )
            )->where(
                'report_table_views.event_type_id = ?',
                $productViewEvent
            )->group(
                'e.entity_id'
            )->order(
                'views DESC'
            )->having(
                'COUNT(report_table_views.event_id) > ?',
                0
            );
            
        $collection->addStoreFilter($this->getStoreId())
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1);
       // ->addViewsCount()

        return $collection;
    }
  
    /**
     * Get auto related products
     *
     * @param $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getAutoRelatedProducts($collection)
    {
        $product = $this->getProduct();
        if (!$product) {
            return;
        }
        $categories = $this->getProduct()->getCategoryIds();
        $collection->setVisibility(
            $this->catalogProductVisibility->getVisibleInCatalogIds()
        );
        
        $collection = $this->_addProductAttributesAndPrices($collection);
        $collection->addCategoriesFilter(['in' => $categories]);
        $collection->addStoreFilter($this->getStoreId())
                    ->setPageSize($this->getProductsCount())
                    ->setCurPage(1);
        $collection->addAttributeToFilter(
            'entity_id',
            ['neq' => $product->getId()]
        );
        $collection->getSelect()->order('rand()');
        
        return $collection;
    }
    
    /**
     * Get on sale slider products
     *
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getOnSaleProducts($collection)
    {
        $inCatalogIds = $this->catalogProductVisibility->getVisibleInCatalogIds();
        $collection->setVisibility($inCatalogIds);
        
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addAttributeToFilter(
                'special_from_date',
                ['date' => true, 'to' => $this->getEndOfDayDate()],
                'left'
            )
            ->addAttributeToFilter(
                'special_to_date',
                [
                    'or' => [
                        0 => ['date' => true, 'from' => $this->getStartOfDayDate()],
                        1 => ['is' => new \Zend_Db_Expr('null')],
                    ]
                ],
                'left'
            )
             ->addAttributeToSort(
                 'news_from_date',
                 'desc'
             )
             ->addStoreFilter($this->getStoreId())
             ->setPageSize($this->getProductsCount())
             ->setCurPage(1);
             
            return $collection;
    }
    
    /**
     * Get slider products including additional products
     *
     * @return array
     */
    public function getSliderProductsCollection()
    {
        $collection = [];
        $type = $this->slider->getType();
        
        $featuredProducts = $this->getSliderProducts("featured");
        if (count($featuredProducts)>0) {
            $collection['featured'] = $featuredProducts;
        }
        
        if ($type !== "featured") {
            if ($this->productsNumber > 0) {
                $sliderProds = $this->getSliderProducts($type);
                if (count($sliderProds)>0) {
                    $collection['products'] = $sliderProds;
                }
            }
        }
        
        return $collection;
    }
    
    /**
     * Create import services form select element
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _prepareLayout()
    {
        $sliderId = $this->getSliderId();
        $enableSwatches = $this->_coreRegistry->registry('enable_swatches');
        if ($enableSwatches) {
            $block = $this->getLayout()->createBlock(
                'Magento\Framework\View\Element\RendererList',
                'details.renderers'.$sliderId
            );
            $this->getLayout()->setChild(
                $this->getNameInLayout(),
                $block->getnameInLayout(),
                'details.renderers'.$sliderId
            );
            $block = $this->getLayout()->createBlock(
                'Magento\Swatches\Block\Product\Renderer\Listing\Configurable',
                'configurable'.$sliderId,
                [
                    'data' => ['slider_id' => $sliderId]
                ]
            )
            ->setTemplate('Ulmod_Productslider::swatches/renderer.phtml');
            $this->getLayout()->setChild(
                'details.renderers'.$sliderId,
                $block->getNameInLayout(),
                'configurable'.$sliderId
            );
            $block = $this->getLayout()->createBlock(
                'Magento\Framework\View\Element\Template',
                'default'.$sliderId
            );
            $this->getLayout()->setChild(
                'details.renderers'.$sliderId,
                $block->getNameInLayout(),
                'default'.$sliderId
            );
        }
        
        return parent::_prepareLayout();
    }
  
    /**
     * Get end of day date
     *
     * @return string
     */
    public function getEndOfDayDate()
    {
        return $this->_localeDate->date()
            ->setTime(23, 59, 59)
            ->format('Y-m-d H:i:s');
    }
  
     /**
      * Get start of day date
      *
      * @return string
      */
    public function getStartOfDayDate()
    {
        return $this->_localeDate->date()
            ->setTime(0, 0, 0)
            ->format('Y-m-d H:i:s');
    }
    
    /**
     * Get slider id
     *
     * @return int
     */
    public function getSliderId()
    {
        if (!$this->slider) {
            return $this->_coreRegistry
             ->registry('slider_id');
        }

        return $this->slider->getId();
    }

    /**
     * Set slider model
     *
     * @param \Ulmod\Productslider\Model\ProductSlider $slider
     *
     * @return this
     */
    public function setSlider($slider)
    {
        $this->slider = $slider;
        
        return $this;
    }
    
    /**
     * Get slider
     *
     * @return \Ulmod\Productslider\Model\ProductSlider
     */
    public function getSlider()
    {
        return $this->slider;
    }
    
    /**
     * @param int
     *
     * @return this
     */
    public function setSliderId($sliderId)
    {
        $this->sliderId = $sliderId;
        $slider = $this->sliderFactory->create()
            ->load($sliderId);
        if ($slider->getId()) {
            $this->setSlider($slider);
            $this->setTemplate($this->_template);
        }
        return $this;
    }
    
    /**
     * @return string
     */
    public function getSliderDisplayId()
    {
        return $this->dateTime->timestamp().$this->getSliderId();
    }
    
    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->storeManager
            ->getStore()->getId();
    }
    
    /**
     * @return int
     */
    public function getProductsCount()
    {
        $items = self::MAX_PRODUCTS_COUNT;
        /**
        * Total number of products in slider must be equal with getProductsNumber
        * If not configured in slider settings then default MAX_PRODUCTS_COUNT is used
        * Additional featured products plus base slider type products equals total slider products
        */
        if (!$this->productsNumber) {
            if ($this->slider->getProductsNumber() > 0
                && $this->slider->getProductsNumber() <= self::MAX_PRODUCTS_COUNT
            ) {
                $items = $this->slider->getProductsNumber();
            }
        } else {
            $items = $this->productsNumber;
        }
        
        return $items;
    }

    /**
     * Get details renderer
     *
     * @param null $type
     * @return bool|\Magento\Framework\View\Element\AbstractBlock
     */
    public function getDetailsRenderer($type = null)
    {
        if ($type === null) {
            $type = 'default';
        }
        
        $rendererList = $this->getDetailsRendererList();
        if ($rendererList) {
            return $rendererList->getRenderer($type, 'default'.$this->getSliderId());
        }
        
        return null;
    }
    
    /**
     * Get post parameters
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product)
    {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                    $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }
    
    /**
     * Retrieve product details html
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return mixed
     */
    public function getProductsliderProductDetailsHtml(
        \Magento\Catalog\Model\Product $product
    ) {
    
        $renderer = $this->getDetailsRenderer(
            $product->getTypeId().$this->getSliderId()
        );
        if ($renderer) {
            $renderer->setProduct($product);
            return $renderer->toHtml();
        }
        
        return '';
    }
    
    /**
     * Get details renderer list
     *
     * @return \Magento\Framework\View\Element\RendererList
     */
    protected function getDetailsRendererList()
    {
        return $this->getDetailsRendererListName() ? $this->getLayout()->getBlock(
            $this->getDetailsRendererListName()
        ) : $this->getChildBlock('details.renderers'.$this->getSliderId());
    }
}
